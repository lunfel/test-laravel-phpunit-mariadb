# Set the base image for subsequent instructions
FROM php:7.3

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install git zip unzip libcurl4-gnutls-dev zlib1g-dev libicu-dev libpng-dev libxml2-dev libzip-dev libbz2-dev openssh-client -yqq

# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install mbstring pdo_mysql curl json intl gd xml zip bz2 opcache

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /app

ADD . /app
