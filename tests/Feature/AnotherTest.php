<?php

namespace Tests\Feature;

use App\Organization;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AnotherTest extends TestCase
{
    use RefreshDatabase;

    /** @var User[] */
    private $users = [];

    /** @var Organization[] */
    private $organizations = [];

    public function testOtherStuff()
    {
        foreach (range(0, 9) as $index) {
            $this->users[] = factory(User::class)->create();
            $this->organizations[] = factory(Organization::class)->create();
        }

        foreach ($this->users as $user) {
            foreach ($this->organizations as $organization) {
                $user->organizations()->save($organization);

                $this->assertDatabaseHas('organization_user', [
                    'user_id' => $user->id,
                    'organization_id' => $organization->id
                ]);
            }
        }
    }
}
