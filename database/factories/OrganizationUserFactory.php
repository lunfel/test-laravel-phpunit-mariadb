<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrganizationUser;
use Faker\Generator as Faker;

$factory->define(OrganizationUser::class, function (Faker $faker) {
    return [
        'organization_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'user_id' => function () {
            return factory(\App\Organization::class)->create()->id;
        }
    ];
});
